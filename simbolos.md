## Símbolos
Descrição

## Tabela de geometrias
| *               | Descrição                                              |
| :-------------: | :-------------------------------------------------: |
|  Oval           | Indica o início e a parada dentro de uma sequência       |
|  Paralelogramas | Indica uma entrada ou saída     |
|  Retângulo      | Indicam ações ou o processo em si.     |
|  Diamantes      | Indicam as decisões que precisam ser tomadas.     |



## Tabela de símbolos

| *     | Símbolo                 | Código        |
| :---: | :-------------------:   | :-------:     |
|  A    | Terminal                | (Texto)       |
|  B    | Operação                | ((Texto))     |
|  C    | Retangular circular     | ([Texto])     |
|  D    | Sub-processo          | [[Texto]]     |
|  E    | Database              | [(Texto)]     |
|  F    | Etiqueta              | >Texto]       |
|  G    | Triangulo             | {Texto}       |
|  H    | Hexagono              | {{Texto}}     |
|  I    | Paralelograma         | [/Texto/]     |
|  J    | Paralelograma         | [\Texto\]     |
|  K    | Trapezoide            | [/Texto\]     |
|  L    | Trapezoide            | [\Texto/]     |
|  M    | Duplo circulo         | (((Texto)))   |
|  N    | Processamento         | [Texto]   |

## Flowchart com os símbolos
```mermaid
---
title: Símbolos
---
flowchart LR;
    A(A...)-->B((B...));
    B-->C([C...]);
    C-->D[[D...]];
    D-->E[(E)]
    E-->F>F]
    F-->G{G}
    G-->H{{H}}
    H-->I[/I/]
    I-->J[\J\]
    J-->K[/K\]
    K-->L[\L/]
    L-->M(((M)))
    M-->N[N...]
```